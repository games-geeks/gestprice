<?php

namespace App\Controller\Admin;

use App\Entity\Prix;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Field\UrlField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ColorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\MoneyField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;


class PrixCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Prix::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            AssociationField::new('jeu'),
            AssociationField::new('enseigne'),
            AssociationField::new('console'),
            UrlField::new('url'),
            MoneyField::new('prixCourant')->setCurrency('EUR'),
            MoneyField::new('prixAncien')->setCurrency('EUR'),
            TextField::new('evolution')
                ->setTextAlign('center'),
            MoneyField::new('prixMin')->setCurrency('EUR'),
            MoneyField::new('prixMax')->setCurrency('EUR'),
            TextField::new('commentaire'),
            DateField::new('createdAt')->onlyOnDetail(),
            DateField::new('updatedAt')->hideOnForm(),
            BooleanField::new('isToCheck')->setValue('1'),
        ];
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setDefaultSort(['updatedAt' => 'DESC']);
    }
}
