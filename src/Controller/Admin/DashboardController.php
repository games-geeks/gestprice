<?php

namespace App\Controller\Admin;


use App\Entity\Jeu;
use App\Entity\Prix;
use App\Entity\Console;
use App\Entity\Enseigne;
use App\Entity\Fabricant;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;

class DashboardController extends AbstractDashboardController
{
    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
        //return parent::index();

        // Option 1. You can make your dashboard redirect to some common page of your backend
        //
        // $adminUrlGenerator = $this->container->get(AdminUrlGenerator::class);
        // return $this->redirect($adminUrlGenerator->setController(OneOfYourCrudController::class)->generateUrl());

        // Option 2. You can make your dashboard redirect to different pages depending on the user
        //
        // if ('jane' === $this->getUser()->getUsername()) {
        //     return $this->redirect('...');
        // }

        // Option 3. You can render some custom template to display a proper dashboard with widgets, etc.
        // (tip: it's easier if your template extends from @EasyAdmin/page/content.html.twig)
        //
        // return $this->render('some/path/my-dashboard.html.twig');
        return $this->render('Admin/dashbard.html.twig');
    }
    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('GestPrice');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoDashboard('Dashboard', 'fa fa-home');
        yield MenuItem::linkToCrud('Fabricant', 'fas fa-list', Fabricant::class);
        yield MenuItem::linkToCrud('Console', 'fas fa-list', Console::class);
        yield MenuItem::linkToCrud('Jeu', 'fas fa-gamepad', Jeu::class);
        yield MenuItem::linkToCrud('Enseigne', 'fas fa-store', Enseigne::class);
        yield MenuItem::linkToCrud('Prix', 'fas fa-euro-sign', Prix::class);
    }
}
