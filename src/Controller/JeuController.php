<?php

namespace App\Controller;

use App\Entity\Jeu;
use App\Repository\JeuRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class JeuController extends AbstractController
{
    #[Route('/jeu', name: 'jeu')]
    public function index(JeuRepository $jeuRepository): Response
    {
        return $this->render('jeu/index.html.twig', [
            'controller_name' => 'JeuController',
            'jeux' => $jeuRepository->findAll(),
        ]);
    }

    #[Route('/jeu/{id<[0-9]+>}', name: 'show_game', methods: ['GET'])]
    public function show(Jeu $game): Response
    {

        return $this->render('jeu/show.html.twig', [
            'controller_name' => 'GameController',
            'game' => $game,
        ]);
    }
}
