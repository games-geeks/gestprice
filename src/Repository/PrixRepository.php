<?php

namespace App\Repository;

use App\Entity\Prix;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Prix|null find($id, $lockMode = null, $lockVersion = null)
 * @method Prix|null findOneBy(array $criteria, array $orderBy = null)
 * @method Prix[]    findAll()
 * @method Prix[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PrixRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Prix::class);
    }

    public function findMinPrixCourant($value)
    {
        return $this->createQueryBuilder('p')
            ->select('p, MIN(p.prixCourant) AS Prix')
            ->innerJoin('p.jeu', 'j')
            ->where('j.id = :search')
            ->andWhere('p.prixCourant != 0')
            ->setParameter('search', $value)
            ->getQuery()->getOneOrNullResult();
        // $query->groupBy('s.user');
        // $query->orderBy('max_score', 'DESC');
    }

    public function findMaxPrix($value)
    {
        return $this->createQueryBuilder('p')
            ->select('p, MAX(p.prixMax) AS Prix')
            ->innerJoin('p.jeu', 'j')
            ->where('j.id = :search')
            ->setParameter('search', $value)
            ->getQuery()->getOneOrNullResult();
        // $query->groupBy('s.user');
        // $query->orderBy('max_score', 'DESC');
    }
    public function findMinPrix($value)
    {
        return $this->createQueryBuilder('p')
            ->select('p, MIN(p.prixMin) AS Prix')
            ->innerJoin('p.jeu', 'j')
            ->where('j.id = :search')
            ->setParameter('search', $value)
            ->getQuery()->getOneOrNullResult();
        // $query->groupBy('s.user');
        // $query->orderBy('max_score', 'DESC');
    }

    // /**
    //  * @return Prix[] Returns an array of Prix objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Prix
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
