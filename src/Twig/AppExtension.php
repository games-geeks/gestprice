<?php

namespace App\Twig;

use Twig\TwigFilter;
use Twig\TwigFunction;
use Twig\Extension\AbstractExtension;
use App\Repository\PrixRepository;

class AppExtension extends AbstractExtension
{
    private $prixRepository;

    public function __construct(PrixRepository $prixRepository)
    {
        $this->prixRepository = $prixRepository;
    }
    public function getFilters(): array
    {
        return [
            // If your filter generates SAFE HTML, you should add a third
            // parameter: ['is_safe' => ['html']]
            // Reference: https://twig.symfony.com/doc/2.x/advanced.html#automatic-escaping
            new TwigFilter('filter_name', [$this, 'doSomething']),
        ];
    }


    public function getFunctions(): array
    {
        return [
            new TwigFunction('PrixCourant', [$this, 'PrixCourant']),
            new TwigFunction('PrixMin', [$this, 'PrixMin']),
            new TwigFunction('PrixMax', [$this, 'PrixMax']),
        ];
    }

    public function PrixCourant($id): float
    {
        $prix[] = $this->prixRepository->findMinPrixCourant($id);
        return ($prix[0]['Prix']) / 100;
    }
    public function PrixMax($id): float
    {
        $prix[] = $this->prixRepository->findMaxPrix($id);
        return ($prix[0]['Prix']) / 100;
    }
    public function PrixMin($id): float
    {
        $prix[] = $this->prixRepository->findMinPrix($id);
        return ($prix[0]['Prix']) / 100;
    }
}
