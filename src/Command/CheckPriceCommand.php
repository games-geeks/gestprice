<?php

namespace App\Command;

use App\Repository\PrixRepository;
use App\Service\CheckSiteService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;

class CheckPriceCommand extends Command
{
    protected static $defaultName = 'checkPrice';
    protected static $defaultDescription = 'Permet de vérifier les prix';

    private $em;
    private $repo;
    private $mailer;
    private $params;

    public function __construct(
        EntityManagerInterface $entityManager,
        PrixRepository $prixRepository,
        MailerInterface $mailer,
        ContainerBagInterface $params
    ) {

        $this->em = $entityManager;
        $this->repo = $prixRepository;
        $this->mailer = $mailer;
        $this->params = $params;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription(self::$defaultDescription)
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $lesPrix = $this->repo->findBy(['isToCheck' => true], ['id' => 'desc']);
        $table = new Table($output);

        $table->setHeaderTitle('Vérification Prix')
            ->setHeaders(['Jeu', 'Enseigne', 'Console', 'Ancien Prix', 'Nouveau Prix']);
        $outputStyleRed = new OutputFormatterStyle('red');
        $output->getFormatter()->setStyle('redt', $outputStyleRed);
        $outputStyleGreen = new OutputFormatterStyle('green');
        $output->getFormatter()->setStyle('greent', $outputStyleGreen);
        foreach ($lesPrix as $prix) {
            //     $io->info($site->getName());
            //     on n'ajoute la ligne que si il y a modification
            $prixEnCours = $prix->getPrixCourant();
            CheckSiteService::checkPrix($prix, $this->em);
            if ($prixEnCours != $prix->getPrixCourant()) {
                if ($prix->getPrixCourant() > $prix->getPrixAncien() && $prix->getPrixCourant() != 0) {
                    $table->addRow([
                        $prix->getJeu(), $prix->getEnseigne(), $prix->getConsole(),
                        $prix->getPrixAncien() / 100, '<redt>' .  $prix->getPrixCourant() / 100 . '</redt>'
                    ]);
                } else {
                    $table->addRow([
                        $prix->getJeu(), $prix->getEnseigne(), $prix->getConsole(),
                        $prix->getPrixAncien() / 100, '<greent>' .  $prix->getPrixCourant() / 100 . '</greent>'
                    ]);
                }
            }
        }

        $table->render();
        $io->success('You have a new command! Now make it your own! Pass --help to see your options.');

        return Command::SUCCESS;
    }
}
