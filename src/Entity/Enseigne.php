<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Traits\Timestampable;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;
use App\Repository\EnseigneRepository;

/**
 * @ORM\Entity(repositoryClass=EnseigneRepository::class)
 * @ORM\Table(name="enseignes")
 * @ORM\HasLifecycleCallbacks
 * @Vich\Uploadable
 */
class Enseigne
{
    use Timestampable;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $logo;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     * @Vich\UploadableField(mapping="logo", fileNameProperty="logo")
     * @var File|null
     */
    private $imageFile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $conditionLivraison;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $recherche;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $champs;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $balise;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $actionSeparateur;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $actionChamp;

    /**
     * @ORM\OneToMany(targetEntity=Prix::class, mappedBy="enseigne")
     */
    private $prixes;

    public function __construct()
    {
        $this->prixes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getLogo(): ?string
    {
        return $this->logo;
    }

    public function setLogo(?string $logo): self
    {
        $this->logo = $logo;

        return $this;
    }

    public function getConditionLivraison(): ?string
    {
        return $this->conditionLivraison;
    }

    public function setConditionLivraison(?string $conditionLivraison): self
    {
        $this->conditionLivraison = $conditionLivraison;

        return $this;
    }

    public function getRecherche(): ?string
    {
        return $this->recherche;
    }

    public function setRecherche(?string $recherche): self
    {
        $this->recherche = $recherche;

        return $this;
    }

    public function getChamps(): ?int
    {
        return $this->champs;
    }

    public function setChamps(?int $champs): self
    {
        $this->champs = $champs;

        return $this;
    }

    public function getBalise(): ?string
    {
        return $this->balise;
    }

    public function setBalise(?string $balise): self
    {
        $this->balise = $balise;

        return $this;
    }

    public function getActionSeparateur(): ?string
    {
        return $this->actionSeparateur;
    }

    public function setActionSeparateur(?string $actionSeparateur): self
    {
        $this->actionSeparateur = $actionSeparateur;

        return $this;
    }

    public function getActionChamp(): ?int
    {
        return $this->actionChamp;
    }

    public function setActionChamp(?int $actionChamp): self
    {
        $this->actionChamp = $actionChamp;

        return $this;
    }

    public function __toString()
    {
        return $this->nom;
    }

    /**
     * @return Collection|Prix[]
     */
    public function getPrixes(): Collection
    {
        return $this->prixes;
    }

    public function addPrix(Prix $prix): self
    {
        if (!$this->prixes->contains($prix)) {
            $this->prixes[] = $prix;
            $prix->setEnseigne($this);
        }

        return $this;
    }

    public function removePrix(Prix $prix): self
    {
        if ($this->prixes->removeElement($prix)) {
            // set the owning side to null (unless already changed)
            if ($prix->getEnseigne() === $this) {
                $prix->setEnseigne(null);
            }
        }

        return $this;
    }

    public function setImageFile(?File $imageFile = null): void
    {
        $this->imageFile = $imageFile;

        if (null !== $imageFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->createdAt = new \DateTime('now');
        }
    }

    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }
}
