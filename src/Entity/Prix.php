<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\PrixRepository;
use App\Entity\Traits\Timestampable;

/**
 * @ORM\Entity(repositoryClass=PrixRepository::class)
 * @ORM\HasLifecycleCallbacks
 */
class Prix
{
    use Timestampable;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $url;

    /**
     * @ORM\Column(type="decimal", precision=8, scale=2, nullable=true)
     */
    private $prixCourant;

    /**
     * @ORM\Column(type="decimal", precision=8, scale=2, nullable=true)
     */
    private $prixAncien;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $evolution;

    /**
     * @ORM\Column(type="decimal", precision=8, scale=2, nullable=true)
     */
    private $prixMax;

    /**
     * @ORM\Column(type="decimal", precision=8, scale=2, nullable=true)
     */
    private $prixMin;

    /**
     * @ORM\ManyToOne(targetEntity=Enseigne::class, inversedBy="prixes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $enseigne;

    /**
     * @ORM\ManyToOne(targetEntity=Jeu::class, inversedBy="prixes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $jeu;

    /**
     * @ORM\ManyToOne(targetEntity=Console::class, inversedBy="prixes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $console;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isToCheck;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $commentaire;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getPrixCourant(): ?string
    {
        return $this->prixCourant;
    }

    public function setPrixCourant(string $prixCourant): self
    {
        $this->prixCourant = $prixCourant;

        return $this;
    }

    public function getPrixAncien(): ?string
    {
        return $this->prixAncien;
    }

    public function setPrixAncien(?string $prixAncien): self
    {
        $this->prixAncien = $prixAncien;

        return $this;
    }

    public function getEvolution(): ?string
    {
        return $this->evolution;
    }

    public function setEvolution(string $evolution): self
    {
        $this->evolution = $evolution;

        return $this;
    }

    public function getPrixMax(): ?float
    {
        return $this->prixMax;
    }

    public function setPrixMax(float $prixMax): self
    {
        $this->prixMax = $prixMax;

        return $this;
    }

    public function getPrixMin(): ?float
    {
        return $this->prixMin;
    }

    public function setPrixMin(float $prixMin): self
    {
        $this->prixMin = $prixMin;

        return $this;
    }

    public function getEnseigne(): ?Enseigne
    {
        return $this->enseigne;
    }

    public function setEnseigne(?Enseigne $enseigne): self
    {
        $this->enseigne = $enseigne;

        return $this;
    }

    public function getJeu(): ?Jeu
    {
        return $this->jeu;
    }

    public function setJeu(?Jeu $jeu): self
    {
        $this->jeu = $jeu;

        return $this;
    }

    public function getConsole(): ?Console
    {
        return $this->console;
    }

    public function setConsole(?Console $console): self
    {
        $this->console = $console;

        return $this;
    }

    public function getIsToCheck(): ?bool
    {
        return $this->isToCheck;
    }

    public function setIsToCheck(bool $isToCheck): self
    {
        $this->isToCheck = $isToCheck;

        return $this;
    }

    public function getCommentaire(): ?string
    {
        return $this->commentaire;
    }

    public function setCommentaire(?string $commentaire): self
    {
        $this->commentaire = $commentaire;

        return $this;
    }
}
