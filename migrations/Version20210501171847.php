<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210501171847 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE consoles ADD fabricant_id INT NOT NULL');
        $this->addSql('ALTER TABLE consoles ADD CONSTRAINT FK_393ADA7DCBAAAAB3 FOREIGN KEY (fabricant_id) REFERENCES fabricants (id)');
        $this->addSql('CREATE INDEX IDX_393ADA7DCBAAAAB3 ON consoles (fabricant_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE consoles DROP FOREIGN KEY FK_393ADA7DCBAAAAB3');
        $this->addSql('DROP INDEX IDX_393ADA7DCBAAAAB3 ON consoles');
        $this->addSql('ALTER TABLE consoles DROP fabricant_id');
    }
}
