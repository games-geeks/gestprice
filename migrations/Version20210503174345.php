<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210503174345 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE prix CHANGE prix_courant prix_courant NUMERIC(8, 2) DEFAULT NULL, CHANGE prix_ancien prix_ancien NUMERIC(8, 2) DEFAULT NULL, CHANGE evolution evolution VARCHAR(255) DEFAULT NULL, CHANGE prix_max prix_max NUMERIC(8, 2) DEFAULT NULL, CHANGE prix_min prix_min NUMERIC(8, 2) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE prix CHANGE prix_courant prix_courant NUMERIC(8, 2) NOT NULL, CHANGE prix_ancien prix_ancien NUMERIC(8, 2) NOT NULL, CHANGE evolution evolution VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE prix_max prix_max NUMERIC(8, 2) NOT NULL, CHANGE prix_min prix_min NUMERIC(8, 2) NOT NULL');
    }
}
