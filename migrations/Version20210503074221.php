<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210503074221 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE prix (id INT AUTO_INCREMENT NOT NULL, enseigne_id INT NOT NULL, jeu_id INT NOT NULL, console_id INT NOT NULL, url VARCHAR(255) NOT NULL, prix_courant VARCHAR(255) NOT NULL, prix_ancien VARCHAR(255) DEFAULT NULL, evolution VARCHAR(255) NOT NULL, prix_max DOUBLE PRECISION NOT NULL, prix_min DOUBLE PRECISION NOT NULL, UNIQUE INDEX UNIQ_F7EFEA5E6C2A0A71 (enseigne_id, jeu_id,console_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE prix ADD CONSTRAINT FK_F7EFEA5E6C2A0A71 FOREIGN KEY (enseigne_id) REFERENCES enseignes (id)');
        $this->addSql('ALTER TABLE prix ADD CONSTRAINT FK_F7EFEA5E8C9E392E FOREIGN KEY (jeu_id) REFERENCES jeux (id)');
        $this->addSql('ALTER TABLE prix ADD CONSTRAINT FK_F7EFEA5E72F9DD9F FOREIGN KEY (console_id) REFERENCES consoles (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE prix');
    }
}
