# Jcé

Jcé est un site Web présentant l'actualité et le travail de l'artiste JCé
<!-- make a readme -->
## Environnement de développement
### Prérequis
 * PHP 8.0
 * Composer
 * Symfony CLI
 * Docker
 * Docker-compose
 * nodejs et npm

 Vous pouvez vérifier les pré-requis (sauf Docker et Docker-compose) avec la commande suivante (de la CLI Symfony): 
 ```bash
 symfony check:recquirements
 ```
 ### Lancer l'environnement de développement
  ```bash
 composer install
 npm install
 npm run build
 docker-compose up -d
 symfony.exe serve -d
 ```

### Ajouter des données de tests
```bash
symfony console doctrine:fixtures:load
 ```
 ## Lancer des tests unitaires
  ```bash
 php ./bin/phpunit --testdox
 ```